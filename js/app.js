/**
 * Created by Trajkovski on 29-Sep-15.
 */
var app = angular.module('websocket-web', [
    'ui.router'
]);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {



    $stateProvider
        .state('chat', {
            url: '/chat',
            templateUrl: '../templates/chat.html',
            controller: 'ChatController'
        });

    $urlRouterProvider.otherwise('/chat');
}]);

