/**
 * Created by Trajkovski on 29-Sep-15.
 */
app.service("ChatService", function($timeout, $q) {
    var service = {};

    service.URL = "http://192.168.1.149:8080/chat";

    var socket = {
        client: null,
        stomp: null
    };

    var listener = $q.defer();
    var connectedListener = $q.defer();

    service.connect = function() {
        socket.client = new SockJS(service.URL);
        socket.stomp = Stomp.over(socket.client);
        socket.stomp.connect({}, function (frame) {
            connectedListener.notify(true);
            console.log('Connected: ' + frame);
            socket.stomp.subscribe('/topic/messages', function (message) {
                console.log("message " + message.body);
                listener.notify(JSON.parse(message.body));
            });
        });
    };

    service.receive = function() {
        return listener.promise;
    };

    service.disconnect = function() {
        if (socket.stomp != null) {
            socket.stomp.disconnect();
        }
        connectedListener.notify(false);
        console.log("Disconnected");
    };

    service.sendName = function(content, nickname) {
        socket.stomp.send("/app/chat", {}, JSON.stringify({'content': content,
            'nickname': nickname}));
    };

    service.receiveStatus = function() {
        return connectedListener.promise;
    };

    return service;
});