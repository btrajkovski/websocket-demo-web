/**
 * Created by Trajkovski on 29-Sep-15.
 */
app.controller('ChatController', function($scope, ChatService) {

    console.log("controller");
    $scope.messages = [];
    $scope.message = "";
    $scope.nickname = "";
    $scope.connected = false;

    $scope.connect = function() {
        ChatService.connect();
        //$scope.connected = true;
    };

    $scope.disconnect = function() {
        ChatService.disconnect();
        //$scope.connected = false;
    };

    $scope.send = function() {
        ChatService.sendName($scope.message, $scope.nickname);
        $scope.message = "";
    };

    ChatService.receive().then(null, null, function(message) {
        $scope.messages.push(message);
    });

    ChatService.receiveStatus().then(null, null, function(status) {
        $scope.connected = status;
    });
});